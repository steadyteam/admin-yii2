<?php

namespace Steady\Admin\Api;

use Steady\Engine\Base\ApiObject;
use Steady\Engine\Models\PhotoModel;

/**
 * @property PhotoModel model
 */
class PhotoObject extends ApiObject
{
    public $modelName = PhotoModel::class;

    public $image;
    public $description;
}