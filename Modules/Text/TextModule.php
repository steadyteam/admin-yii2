<?php

namespace Steady\Admin\Modules\Text;

use Steady\Admin\Components\SteadyModule;

class TextModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'text',
        'class' => self::class,
        'title' => [
            'en' => 'Text',
            'ru' => 'Текст',
        ],
        'icon' => 'font',
        'order_num' => 85,
    ];
}