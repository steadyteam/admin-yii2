<?php

use Steady\Engine\SW;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>
<?= $form->field($model, 'alias') ?>
<?= $form->field($model, 'text')->textarea() ?>
<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>