<?php

namespace Steady\Admin\Modules\System\Controllers;

use Steady\Admin\AdminModule;
use Steady\Admin\Components\AdminController;
use Steady\Engine\Helpers\WebConsole;
use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;

class AController extends AdminController
{
    /**
     * @var array
     */
    public $rootActions = 'all';

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     * @throws \yii\web\ServerErrorHttpException
     */
    public function actionUpdate()
    {
        $webConsole = new WebConsole(SW::getAlias('@install/Config/install-console.php'));
        $migrationResult = $webConsole->migrate();

        SettingModel::set('steady_version', AdminModule::VERSION);

        return $this->render('update', ['result' => $migrationResult]);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     * @throws \yii\web\ServerErrorHttpException
     */
    public function actionVersionDown()
    {
        $webConsole = new WebConsole(SW::getAlias('@install/Config/install-console.php'));
        $migrationResult = $webConsole->migrateDown();

        SettingModel::set('steady_version', AdminModule::VERSION);

        return $this->render('update', ['result' => $migrationResult]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionFlushCache()
    {
        SW::$app->cache->flush();
        $this->flash('success', SW::t('admin', 'Cache flushed'));
        return $this->back();
    }

    /**
     * @return \yii\web\Response
     */
    public function actionClearAssets()
    {
        foreach (glob(SW::$app->assetManager->basePath . DIRECTORY_SEPARATOR . '*') as $asset) {
            if (is_link($asset)) {
                unlink($asset);
            } else if (is_dir($asset)) {
                $this->deleteDir($asset);
            } else {
                unlink($asset);
            }
        }
        $this->flash('success', SW::t('admin', 'Assets cleared'));
        return $this->back();
    }

    /**
     * @name $id
     */
    public function actionLiveEdit($id)
    {
        SW::$app->session->set('steady_live_edit', $id);
        $this->back();
    }

    /**
     * @name $directory
     * @return bool
     */
    private function deleteDir($directory)
    {
        $iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        return rmdir($directory);
    }
}