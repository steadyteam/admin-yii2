<?php

namespace Steady\Admin\Modules\System;

use Steady\Admin\Components\SteadyModule;

class SystemModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'system',
        'class' => self::class,
        'title' => [
            'en' => 'System',
            'ru' => 'Система',
        ],
        'icon' => 'hdd',
        'order_num' => 60,
    ];
}