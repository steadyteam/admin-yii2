<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$this->title = SW::t('admin', 'Update');
?>
<ul class="nav nav-pills">
    <li>
        <a href="<?= Url::to(['/admin/system']) ?>">
            <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?= SW::t('admin', 'Back') ?>
        </a>
    </li>
</ul>
<br>

<pre>
<?= $result ?>
</pre>
