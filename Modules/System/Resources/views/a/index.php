<?php

use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use yii\helpers\Url;

$this->title = SW::t('admin', 'System');
?>

<h4>
    <?= SW::t('admin', 'Current version') ?>: <b><?= SettingModel::get('steady_version') ?></b>
</h4>
<br>

<p>
    <a href="<?= Url::to(['/admin/system/a/update']) ?>" class="btn btn-success">
        <?= SW::t('admin', 'Update') ?>
    </a>
</p>
<br>

<p>
    <a href="<?= Url::to(['/admin/system/a/flush-cache']) ?>" class="btn btn-default">
        <i class="glyphicon glyphicon-flash"></i> <?= SW::t('admin', 'Flush cache') ?>
    </a>
</p>
<br>

<p>
    <a href="<?= Url::to(['/admin/system/a/clear-assets']) ?>" class="btn btn-default">
        <i class="glyphicon glyphicon-trash"></i> <?= SW::t('admin', 'Clear assets') ?>
    </a>
</p>
<br>