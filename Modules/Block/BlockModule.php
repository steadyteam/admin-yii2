<?php

namespace Steady\Admin\Modules\Block;

use Steady\Admin\Components\SteadyModule;

class BlockModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'block',
        'class' => self::class,
        'title' => [
            'en' => 'Blocks',
            'ru' => 'Блоки',
        ],
        'icon' => 'th-large',
        'order_num' => 90,
    ];

    public static $moduleConfig = [
        'runtimeViewPath' => '@runtime/views/blocks',
    ];
}