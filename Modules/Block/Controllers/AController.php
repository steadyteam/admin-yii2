<?php

namespace Steady\Admin\Modules\Block\Controllers;

use ImagickException;
use Steady\Admin\Components\TreeController;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Modules\Block\Models\BlockModel;
use yii\base\Exception;
use yii\web\HttpException;

class AController extends TreeController
{
    public $modelName = BlockModel::class;

    public $moduleName = 'block';

    public $linkRoute = '/a/edit';

    public $rootActions = ['create', 'delete'];

    /** @inheritdoc
     * @name TreeModel $model
     * @throws ImagickException
     * @throws Exception
     * @throws HttpException
     */
    public function beforeSave($model)
    {
        if (isset($_FILES)) {
            $this->saveImage($model);
        }

        parent::beforeSave($model);
    }
}