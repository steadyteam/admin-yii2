<?php

use Steady\Engine\SW;
use Steady\Modules\Catalog\Assets\FieldsAsset;
use Steady\Modules\Catalog\Models\CategoryModel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = SW::t('admin', 'Category fields');

$this->registerAssetBundle(FieldsAsset::className());

$this->registerJs('
var fieldTemplate = \'\
    <tr>\
        <td>' . Html::input('text', null, null, ['class' => 'form-control field-title']) . '</td>\
        <td>\
            <select class="form-control field-type">' . str_replace("\n", "", Html::renderSelectOptions('', CategoryModel::$fieldTypes)) . '</select>\
        </td>\
        <td>' . Html::input('text', null, null, ['class' => 'form-control field-slug']) . '</td>\
        <td class="text-right">\
            <div class="btn-group btn-group-sm" role="group">\
                <!--<a href="#" class="btn btn-default move-up" title="' . SW::t('admin', 'Move up') . '"><span class="glyphicon glyphicon-arrow-up"></span></a>-->\
                <!--<a href="#" class="btn btn-default move-down" title="' . SW::t('admin', 'Move down') . '"><span class="glyphicon glyphicon-arrow-down"></span></a>-->\
                <a href="#" class="btn btn-default color-red delete-field" title="' . SW::t('admin', 'Delete item') . '"><span class="glyphicon glyphicon-remove"></span></a>\
            </div>\
        </td>\
    </tr>\';
', \yii\web\View::POS_HEAD);

?>
<?= $this->render('_menu') ?>
<?= $this->render('_submenu', ['model' => $model]) ?>
<br>

<?= Html::button('<i class="glyphicon glyphicon-plus font-12"></i> ' .
    SW::t('admin', 'Add field'), ['class' => 'btn btn-default', 'id' => 'addField']) ?>

<table id="categoryFields" class="table table-hover">
    <thead>
    <th><?= SW::t('admin', 'Title') ?></th>
    <th><?= SW::t('admin', 'Type') ?></th>
    <th><?= SW::t('admin', 'Slug') ?></th>
    <th width="120"></th>
    </thead>
    <tbody>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'options' => ['class' => 'model-form'],
    ]); ?>
    <?php foreach ($fields as $i => $field) : ?>
        <?php print_r($field);
        exit; ?>
        <tr>
            <td><?= $form->field($field, 'title')->label(false) ?></td>
            <td><?= $form->field($field, 'type')->label(false)->dropDownList(CategoryModel::$fieldTypes) ?></td>
            <td><?= $form->field($field, 'slug')->label(false) ?></td>
            <td class="text-right">
                <div class="btn-group btn-group-sm" role="group">
                    <a href="#" class="btn btn-default color-red delete-field"
                       title="<?= SW::t('admin', 'Delete item') ?>">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    <?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>
    </tbody>
</table>
