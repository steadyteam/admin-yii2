<?php

use Steady\Admin\Helpers\HtmlHelper;
use Steady\Admin\Widgets\RedactorWidget;
use Steady\Admin\Widgets\SeoFormWidget;
use Steady\Engine\Helpers\Image;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'alias') ?>
<?/*= $form->field($model, 'layout_id')->dropDownList(ArrayHelper::map($layoutsIds, 'layout_id', 'title'),
    ['prompt' => '- SelectWidget layout -']) */?>
<?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($validParents, 'block_id', 'title'),
    ['prompt' => '- SelectWidget block -']) ?>
<?= $form->field($model, 'type')->dropDownList($model->types()) ?>
<?= $form->field($model, 'slug') ?>

<?php if ($model->image) : ?>
    <? $clearUrl = Url::to(['/admin/' . $this->context->moduleName . '/a/clear-image', 'id' => $model->primaryKey]) ?>
    <? $clearText = SW::t('admin', 'Clear image') ?>
    <div class="form-group">
        <img src="<?= Image::thumb($model->image, 240) ?>">
    </div>
    <div class="form-group">
        <a href="<?= $clearUrl ?>" class="text-danger confirm-delete" title="<?= $clearText ?>">
            <?= $clearText ?>
        </a>
    </div>
<?php endif; ?>
<?= $form->field($model, 'image')->fileInput() ?>

<?= $form->field($model, 'preview')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 100,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
    ],
]) ?>
<?= $form->field($model, 'content')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 200,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'plugins' => ['fullscreen'],
    ],
]) ?>

<?= $form->field($model, 'params')->textarea() ?>

<?= HtmlHelper::generateFieldForm($model) ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>