<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/block', 'Edit block');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>