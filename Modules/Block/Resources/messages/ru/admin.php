<?php
return [
    'Blocks' => 'Блоки',
    'Create block' => 'Создать блок',
    'Edit block' => 'Редактировать блок',
    'Block created' => 'Блок успешно создан',
    'Block updated' => 'Блок обновлен',
    'Block deleted' => 'Блок удален',

    'Parent block' => 'Родительский блок',
];