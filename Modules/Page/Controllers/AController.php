<?php

namespace Steady\Admin\Modules\Page\Controllers;

use Steady\Admin\Components\TreeController;
use Steady\Engine\Modules\Layout\Models\LayoutModel;
use Steady\Engine\Modules\Page\Models\PageModel;

class AController extends TreeController
{
    public $modelName = PageModel::class;

    public $moduleName = 'page';

    public $linkRoute = '/a/edit';

    public $rootActions = ['create', 'delete'];

    /**
     * @inheritdoc
     * @name PageModel $model
     * @return void
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function beforeSave($model)
    {
        if (isset($_FILES)) {
            $this->saveImage($model);
        }

        parent::beforeSave($model);
    }

    /**
     * @inheritdoc
     */
    public function actionCreate($parent_id = null, $slug = null)
    {
        $task = $this->task(self::TASK_CUSTOM_RENDER);

        $result = parent::actionCreate($parent_id, $slug);

        if ($result === $task) {
            $task->data['form']['layoutsIds'] = LayoutModel::tree(true);
            $result = $this->render('create', $task->data);
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function actionEdit($id)
    {
        $task = $this->task(self::TASK_CUSTOM_RENDER);

        $result = parent::actionEdit($id);

        if ($result === $task) {
            $task->data['form']['layoutsIds'] = LayoutModel::tree(true);
            $result = $this->render('edit', $task->data);
        }
        return $result;
    }
}