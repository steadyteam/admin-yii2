<?php

namespace Steady\Admin\Modules\Page;

use Steady\Admin\Components\SteadyModule;

class PageModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'page',
        'class' => self::class,
        'title' => [
            'en' => 'Pages',
            'ru' => 'Страницы',
        ],
        'icon' => 'file',
        'order_num' => 95,
    ];

    public static $moduleConfig = [
        'runtimeViewPath' => '@runtime/views/pages',
    ];
}