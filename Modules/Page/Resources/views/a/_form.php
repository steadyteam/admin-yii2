<?php

use Steady\Admin\Helpers\HtmlHelper;
use Steady\Admin\Widgets\RedactorWidget;
use Steady\Admin\Widgets\SeoFormWidget;
use Steady\Engine\Helpers\Image;
use Steady\Engine\Modules\Page\Models\PageModel;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var PageModel $model
 * @var array $layoutsIds
 * @var array $validParents
 */

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>

<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'alias') ?>
<?= $form->field($model, 'layout_id')->dropDownList(ArrayHelper::map($layoutsIds, 'layout_id', 'title')) ?>
<?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($validParents, 'page_id', 'title')) ?>
<?= $form->field($model, 'type')->dropDownList($model->types()) ?>

<?php if ($model->image) : ?>
    <? $clearUrl = Url::to(['/admin/' . $this->context->moduleName . '/a/clear-image', 'id' => $model->primaryKey]) ?>
    <? $clearText = SW::t('admin', 'Clear image') ?>
    <div class="form-group">
        <img src="<?= Image::thumb($model->image, 240) ?>">
    </div>
    <div class="form-group">
        <a href="<?= $clearUrl ?>" class="text-danger confirm-delete" title="<?= $clearText ?>">
            <?= $clearText ?>
        </a>
    </div>
<?php endif; ?>
<?= $form->field($model, 'image')->fileInput() ?>

<?= $form->field($model, 'slug') ?>
<?= $form->field($model, 'route') ?>
<?= $form->field($model, 'preview')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 100,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
    ],
]) ?>
<?= $form->field($model, 'content')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 300,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
        'plugins' => ['fullscreen'],
    ],
]) ?>

<?= $form->field($model, 'params')->textarea() ?>

<?= HtmlHelper::generateFieldForm($model) ?>

<?= SeoFormWidget::widget(['model' => $model]) ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>