<?php

use Steady\Engine\Modules\Page\Models\PageModel;
use Steady\Engine\SW;
use yii\helpers\Url;

/**
 * @var PageModel $model
 */

$action = $this->context->action->id;
$module = $this->context->module->id;

?>

<div class="action-list">
    <?php if ($action != 'index') : ?>
        <a class="btn btn-primary" href="<?= $this->context->getReturnUrl(["/admin/$module"]) ?>">
            <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?= SW::t('admin', 'Back') ?>
        </a>
        <?php if (isset($model->primaryKey)) : ?>
            <a class="btn btn-success" href="<?= Url::to(['/admin/field/a/add-assign',
                'owner_id' => $model->primaryKey,
                'class_alias' => $model->getAlias()]) ?>">
                <?= SW::t('admin', 'Add field') ?>
            </a>
        <?php endif; ?>
    <?php else : ?>
        <a class="btn btn-success" href="<?= Url::to(["/admin/$module/a/create"]) ?>">
            <?= SW::t('admin', 'Create') ?>
        </a>
    <?php endif; ?>
</div>
<br/>
