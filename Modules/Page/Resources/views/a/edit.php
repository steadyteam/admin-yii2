<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/page', 'Edit page');

?>

<?= $this->render('_menu', $form) ?>
<?= $this->render('_form', $form) ?>