<?php

namespace Steady\Admin\Modules\Module;

use Steady\Admin\Components\SteadyModule;

class ModuleModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'module',
        'class' => self::class,
        'title' => [
            'en' => 'Modules',
            'ru' => 'Модули',
        ],
        'icon' => 'folder-close',
        'order_num' => 65,
    ];
}