<?php

use Steady\Engine\SW;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->title;
?>
<?= $this->render('_menu') ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<?php $form = ActiveForm::begin(['enableAjaxValidation' => true]) ?>
<?= $form->field($formModel, 'title') ?>
<?= $form->field($formModel, 'name') ?>
<?= Html::submitButton(SW::t('admin', 'Copy'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>