<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'edit') ? 'class="active"' : '' ?>><a
                href="<?= Url::to(['/admin/module/a/edit/', 'id' => $model->primaryKey]) ?>"> <?= SW::t('admin', 'Basic') ?></a>
    </li>
    <li <?= ($action === 'settings') ? 'class="active"' : '' ?>><a
                href="<?= Url::to(['/admin/module/a/settings/', 'id' => $model->primaryKey]) ?>"><i
                    class="glyphicon glyphicon-cog"></i> <?= SW::t('admin', 'Advanced') ?></a></li>
    <li class="pull-right <?= ($action === 'copy') ? 'active' : '' ?>"><a
                href="<?= Url::to(['/admin/module/a/copy/', 'id' => $model->primaryKey]) ?>"
                class="text-muted"><?= SW::t('admin', 'Copy') ?></a></li>
</ul>
<br>