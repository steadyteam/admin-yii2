<?php

use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Engine\SW;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = SW::t('admin', 'Modules');
?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'Name') ?></th>
            <th><?= SW::t('admin', 'Title') ?></th>
            <th width="150"><?= SW::t('admin', 'Icon') ?></th>
            <th width="100"><?= SW::t('admin', 'Status') ?></th>
            <th width="150"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $module) : ?>
            <tr>
                <td><?= $module->primaryKey ?></td>
                <td><a href="<?= Url::to(['/admin/module/a/edit/', 'id' => $module->primaryKey]) ?>"
                       title="<?= SW::t('admin', 'Edit') ?>"><?= $module->name ?></a></td>
                <td><?= $module->title ?></td>
                <td>
                    <?php if ($module->icon) : ?>
                        <span class="glyphicon glyphicon-<?= $module->icon ?>"></span> <?= $module->icon ?>
                    <?php endif; ?>
                </td>
                <td class="status">
                    <?= Html::checkbox('', $module->status == ModuleModel::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $module->primaryKey,
                        'data-link' => Url::to(['/admin/module/a/']),
                        'data-reload' => '1',
                    ]) ?>
                </td>
                <td class="control">
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="<?= Url::to(['/admin/module/a/up/', 'id' => $module->primaryKey]) ?>"
                           class="btn btn-default" title="<?= SW::t('admin', 'Move up') ?>"><span
                                    class="glyphicon glyphicon-arrow-up"></span></a>
                        <a href="<?= Url::to(['/admin/module/a/down/', 'id' => $module->primaryKey]) ?>"
                           class="btn btn-default" title="<?= SW::t('admin', 'Move down') ?>"><span
                                    class="glyphicon glyphicon-arrow-down"></span></a>
                        <a href="<?= Url::to(['/admin/module/a/delete/', 'id' => $module->primaryKey]) ?>"
                           class="btn btn-default confirm-delete" title="<?= SW::t('admin', 'Delete item') ?>"><span
                                    class="glyphicon glyphicon-remove"></span></a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $data->pagination,
        ]) ?>
    </table>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>