<?php

namespace Steady\Admin\Modules\Module\Controllers;

use Steady\Admin\Behaviors\SortableControllerBehavior;
use Steady\Admin\Behaviors\StatusControllerBehavior;
use Steady\Admin\Components\AdminController;
use Steady\Admin\Modules\Module\Forms\CopyModuleForm;
use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Engine\SW;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;
use yii\widgets\ActiveForm;

/**
 * @mixin SortableControllerBehavior
 * @mixin StatusControllerBehavior
 */
class AController extends AdminController
{
    public $rootActions = 'all';

    public function behaviors()
    {
        return [
            [
                'class' => SortableControllerBehavior::class,
                'model' => ModuleModel::class,
            ],
            [
                'class' => StatusControllerBehavior::class,
                'model' => ModuleModel::class,
            ],
        ];
    }

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => ModuleModel::find()->sort(),
        ]);
        SW::$app->user->setReturnUrl('/admin/module');

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionCreate()
    {
        $model = new ModuleModel;

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin', 'Module created'));
                    return $this->redirect(['/admin/module']);
                } else {
                    $this->flash('error', SW::t('Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = ModuleModel::findOne($id);

        if ($model === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
            return $this->redirect(['/admin/module']);
        }

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin', 'Module updated'));
                } else {
                    $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        } else {
            return $this->render('edit', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    public function actionSettings($id)
    {
        $model = ModuleModel::findOne($id);

        if ($model === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
            return $this->redirect(['/admin/module']);
        }

        if (SW::$app->request->post('Settings')) {
            $model->setSettings(SW::$app->request->post('Settings'));
            if ($model->save()) {
                $this->flash('success', SW::t('admin', 'Module settings updated'));
            } else {
                $this->flash('error', SW::t('admin', SW::t('admin', 'Update error. {0}', $model->formatErrors())));
            }
            return $this->refresh();
        } else {

            return $this->render('settings', [
                'model' => $model,
            ]);
        }
    }

    public function actionRestoreSettings($id)
    {
        if (($model = ModuleModel::findOne($id))) {
            $model->settings = '';
            $model->save();
            $this->flash('success', SW::t('admin', 'Module default settings was restored'));
        } else {
            $this->flash('error', SW::t('admin', 'Not found'));
        }
        return $this->back();
    }

    /**
     * @name $id
     * @return array|string|\yii\web\Response
     * @throws \ReflectionException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public function actionCopy($id)
    {
        /** @var ModuleModel $module */
        $module = ModuleModel::findOne($id);
        $formModel = new CopyModuleForm();

        if ($module === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
            return $this->redirect('/admin/module');
        }
        if ($formModel->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($formModel);
            } else {
                $reflector = new \ReflectionClass($module->class);
                $oldModuleFolder = dirname($reflector->getFileName());
                $oldNameSpace = $reflector->getNamespaceName();
                $oldModuleClass = $reflector->getShortName();

                $newModulesFolder = SW::getAlias('@app') . DIRECTORY_SEPARATOR . 'modules';
                $newModuleFolder = SW::getAlias('@app') . DIRECTORY_SEPARATOR . 'modules'
                    . DIRECTORY_SEPARATOR . $formModel->name;
                $newNameSpace = 'app\modules\\' . $formModel->name;
                $newModuleClass = ucfirst($formModel->name) . 'Module';

                if (!FileHelper::createDirectory($newModulesFolder, 0755)) {
                    $this->flash('error', 'Cannot create `' . $newModulesFolder
                        . '`. Please check write permissions.');
                    return $this->refresh();
                }

                if (file_exists($newModuleFolder)) {
                    $this->flash('error', 'New module folder `' . $newModulesFolder . '` already exists.');
                    return $this->refresh();
                }

                //Copying module folder
                try {
                    FileHelper::copyDirectory($oldModuleFolder, $newModuleFolder);
                } catch (\Exception $e) {
                    $this->flash('error', 'Cannot copy `' . $oldModuleFolder . '` to `'
                        . $newModuleFolder . '`. Please check write permissions.');
                    return $this->refresh();
                }

                //Renaming module file name
                $newModuleFile = $newModuleFolder . DIRECTORY_SEPARATOR . $newModuleClass . '.php';
                $oldModuleFile = $newModuleFolder . DIRECTORY_SEPARATOR . $reflector->getShortName() . '.php';

                if (!rename($oldModuleFile, $newModuleFile)) {
                    $this->flash('error', 'Cannot rename `' . $newModulesFolder . '`.');
                    return $this->refresh();
                }

                //Renaming module class name
                $moduleFileContent = file_get_contents($newModuleFile);
                $moduleFileContent = str_replace($oldModuleClass, $newModuleClass, $moduleFileContent);
                $moduleFileContent = str_replace('@admin', '@app', $moduleFileContent);
                $moduleFileContent = str_replace('/' . $module->name, '/' . $formModel->name, $moduleFileContent);
                file_put_contents($newModuleFile, $moduleFileContent);

                //Replacing namespace
                foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($newModuleFolder)) as $file => $object) {
                    if (!$object->isDir()) {
                        $fileContent = file_get_contents($file);
                        $fileContent = str_replace($oldNameSpace, $newNameSpace, $fileContent);
                        $fileContent = str_replace("Steady::t('admin/" . $module->name, "Steady::t('admin/"
                            . $formModel->name, $fileContent);
                        $fileContent = str_replace("'" . $module->name . "'", "'" . $formModel->name
                            . "'", $fileContent);

                        file_put_contents($file, $fileContent);
                    }
                }

                //Copying module tables
                foreach (glob($newModuleFolder . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . '*.php')
                         as $modelFile) {
                    $baseName = basename($modelFile, '.php');
                    $modelClass = $newNameSpace . '\models\\' . $baseName;

                    $oldTableName = $modelClass::tableName();
                    $newTableName = str_replace($module->name, $formModel->name, $oldTableName);
                    $newTableName = str_replace('steady', 'app', $newTableName);

                    try {
                        //Drop new table if exists
                        SW::$app->db->createCommand("DROP TABLE IF EXISTS `$newTableName`;")->execute();

                        //Copy new table
                        SW::$app->db->createCommand("CREATE TABLE `$newTableName` LIKE `$oldTableName`;")->execute();
                    } catch (\yii\db\Exception $e) {
                        $this->flash('error', 'Copy table error. ' . $e);
                        FileHelper::removeDirectory($newModuleFolder);
                        return $this->refresh();
                    }

                    file_put_contents($modelFile, str_replace($oldTableName, $newTableName, file_get_contents($modelFile)));
                }

                $newModule = new ModuleModel([
                    'name' => $formModel->name,
                    'class' => $newNameSpace . '\\' . $newModuleClass,
                    'title' => $formModel->title,
                    'icon' => $module->icon,
                    'settings' => $module->settings,
                    'status' => ModuleModel::STATUS_ON,
                ]);

                if ($newModule->save()) {
                    $this->flash('success', 'New module created');
                    return $this->redirect(['/admin/module/a/edit', 'id' => $newModule->primaryKey]);
                } else {
                    $this->flash('error', 'Module create error. ' . $newModule->formatErrors());
                    FileHelper::removeDirectory($newModuleFolder);
                    return $this->refresh();
                }
            }
        }


        return $this->render('copy', [
            'model' => $module,
            'formModel' => $formModel,
        ]);
    }

    /**
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (($model = ModuleModel::findOne($id))) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse(SW::t('admin', 'Module deleted'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, ModuleModel::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, ModuleModel::STATUS_OFF);
    }
}