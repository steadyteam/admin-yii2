<?php

namespace Steady\Admin\Modules\Module\Forms;

use Steady\Admin\Components\SteadyModule;
use Steady\Engine\Base\Form;
use Steady\Engine\SW;

class CopyModuleForm extends Form
{
    public $title;
    public $name;

    public function rules()
    {
        return [
            [['title', 'name'], 'required'],
            ['name', 'match', 'pattern' => '/^[\w]+$/'],
            ['name', 'unique', 'targetClass' => SteadyModule::class],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'New module name',
            'title' => SW::t('admin', 'Title'),
        ];
    }
}