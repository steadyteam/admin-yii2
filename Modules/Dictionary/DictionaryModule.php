<?php

namespace Steady\Admin\Modules\Dictionary;

use Steady\Admin\Components\SteadyModule;

class DictionaryModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'dictionary',
        'class' => self::class,
        'title' => [
            'en' => 'Dictionaries',
            'ru' => 'Словари',
        ],
        'icon' => 'list',
        'order_num' => 75,
    ];
}