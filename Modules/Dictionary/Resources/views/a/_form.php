<?php

use Steady\Engine\Modules\Dictionary\Models\DictionaryModel;
use Steady\Engine\SW;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var DictionaryModel $model
 */

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>

<?= $form->field($model, 'alias') ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'value') ?>
<?= $form->field($model, 'folder')->checkbox() ?>
<?= $form->field($model, 'settings')->textarea() ?>
<?= $form->field($model, 'params')->textarea() ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>