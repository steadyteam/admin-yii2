<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$this->title = SW::t('admin/dictionary', 'Dictionary');

$module = $this->context->module->id;

?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'Alias') ?></th>
            <th><?= SW::t('admin', 'Text') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $item) : ?>
            <tr>
                <td><?= $item->primaryKey ?></td>
                <td>
                    <a href="<?= Url::to(['/admin/' . $module . '/a/edit', 'id' => $item->primaryKey]) ?>"><?= $item->alias ?></a>
                </td>
                <td><?= $item->text ?></td>
                <td>
                    <a href="<?= Url::to(['/admin/' . $module . '/a/delete', 'id' => $item->primaryKey]) ?>"
                       class="glyphicon glyphicon-remove confirm-delete"
                       title="<?= SW::t('admin', 'Delete item') ?>"></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination,
    ]) ?>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>