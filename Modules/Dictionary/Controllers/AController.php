<?php

namespace Steady\Admin\Modules\Dictionary\Controllers;

use Steady\Admin\Components\TreeController;
use Steady\Admin\Modules\Dictionary\DictionaryModule;

class AController extends TreeController
{
    public $modelName = DictionaryModule::class;

    public $moduleName = 'dictionary';

    public $linkRoute = '/a/edit';

    public $rootActions = ['create', 'delete'];

}