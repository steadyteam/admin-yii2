<?php

namespace Steady\Admin\Modules\Log\Controllers;

use Steady\Admin\Components\AdminController;
use Steady\Engine\Modules\User\Models\AuthModel;
use yii\data\ActiveDataProvider;

class LogsController extends AdminController
{
    public $rootActions = 'all';

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => AuthModel::find()->desc(),
        ]);

        return $this->render('index', [
            'data' => $data,
        ]);
    }
}