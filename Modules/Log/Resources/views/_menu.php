<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
?>
<ul class="nav nav-pills">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>><a
                href="<?= Url::to(['/admin/logs']) ?>"><?= SW::t('admin', 'Sign in') ?></a></li>
</ul>
<br/>
