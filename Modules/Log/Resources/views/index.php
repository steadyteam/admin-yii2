<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Logs');
?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'Username') ?></th>
            <th>IP</th>
            <th>USER AGENT</th>
            <th><?= SW::t('admin', 'Date') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $log) : ?>
            <tr <?= !$log->success ? 'class="danger"' : '' ?>>
                <td><?= $log->primaryKey ?></td>
                <td><?= $log->username ?></td>
                <td><?= $log->ip ?></td>
                <td><?= $log->user_agent ?></td>
                <td><?= SW::$app->formatter->asDatetime($log->time, 'medium') ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination,
    ]) ?>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>