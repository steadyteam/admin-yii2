<?php

namespace Steady\Admin\Modules\User;

use Steady\Admin\Components\SteadyModule;

class UserModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'user',
        'class' => self::class,
        'title' => [
            'en' => 'Users',
            'ru' => 'Пользователи',
        ],
        'icon' => 'users',
        'order_num' => 60,
    ];
}