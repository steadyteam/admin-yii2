<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$this->title = SW::t('admin', 'Users');
?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'Username') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $user) : ?>
            <tr <?php if ($user->isAdmin()) { ?>class="success"<?php } ?>>
                <td><?= $user->user_id ?></td>
                <td>
                    <a href="<?= Url::to(['/admin/user/a/edit', 'id' => $user->user_id]) ?>"><?= $user->username ?></a>
                </td>
                <td><a href="<?= Url::to(['/admin/user/a/delete', 'id' => $user->user_id]) ?>"
                       class="glyphicon glyphicon-remove confirm-delete"
                       title="<?= SW::t('admin', 'Delete item') ?>"></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $data->pagination,
        ]) ?>
    </table>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>