<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Edit user');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>