<?php

namespace Steady\Admin\Modules\User\Controllers;

use Steady\Admin\Components\AdminController;
use Steady\Engine\Modules\User\Models\UserModel;
use Steady\Engine\SW;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;

class AController extends AdminController
{
    public $rootActions = 'all';

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => UserModel::find()->desc(),
        ]);
        SW::$app->user->setReturnUrl(['/admin/user']);

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionCreate()
    {
        $model = new UserModel;
        $model->scenario = 'create';

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin', 'User created'));
                    return $this->redirect(['/admin/user']);
                } else {
                    $this->flash('error', SW::t('admin', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = UserModel::findOne($id);

        if ($model === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
            return $this->redirect(['/admin/user']);
        }

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin', 'User updated'));
                } else {
                    $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        } else {
            return $this->render('edit', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    /**
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = UserModel::findOne($id);
        if ($model) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse(SW::t('admin', 'User deleted'));
    }
}