<?php

use Steady\Engine\Modules\Field\Models\FieldModel;
use Steady\Engine\SW;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var FieldModel $model
 */

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>

<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'alias') ?>
<?= $form->field($model, 'type')->dropDownList($model->types()) ?>
<?= $form->field($model, 'required')->checkbox() ?>
<?= $form->field($model, 'settings')->textarea() ?>
<?= $form->field($model, 'params')->textarea() ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>