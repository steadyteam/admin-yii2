<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/text', 'Create text');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>