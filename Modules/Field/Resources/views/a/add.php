<?php

use Steady\Engine\Modules\Field\Models\FieldAssignModel;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var FieldAssignModel $model
 * @var array $fieldsIds
 */

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>
<?= $form->field($model, 'field_id')->dropDownList(ArrayHelper::map($fieldsIds, 'field_id', 'title')) ?>
<?= $form->field($model, 'owner_id') ?>
<?= $form->field($model, 'class_alias') ?>
<?= $form->field($model, 'content') ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>