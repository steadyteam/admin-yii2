<?php

namespace Steady\Admin\Modules\Field\Controllers;

use Steady\Admin\Components\TreeController;
use Steady\Engine\Modules\Field\Models\FieldAssignModel;
use Steady\Engine\Modules\Field\Models\FieldModel;
use Steady\Engine\SW;
use yii\bootstrap\ActiveForm;

class AController extends TreeController
{
    public $modelName = FieldModel::class;

    public $moduleName = 'field';

    public $linkRoute = '/a/edit';

    public $rootActions = ['create', 'delete'];

    /**
     * @param null $field_id
     * @param null $owner_id
     * @param null $class_alias
     * @param null $content
     * @return array|string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionAddAssign($field_id = null, $owner_id = null, $class_alias = null, $content = null)
    {
        $model = new FieldAssignModel();
        $model->field_id = $field_id;
        $model->owner_id = $owner_id;
        $model->class_alias = $class_alias;
        $model->content = $content;

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin', 'Field added'));
                    return $this->redirect(['/admin/field']);
                } else {
                    $this->flash('error', SW::t('admin', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('add', [
                'model' => $model,
                'fieldsIds' => FieldModel::tree(false),
            ]);
        }
    }

    /**
     * @param $field_assign_id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteAssign($field_assign_id)
    {
        /** @var FieldAssignModel $model */
        $model = FieldAssignModel::findOne($field_assign_id);

        if ($model) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse(SW::t('admin', 'Field deleted'));
    }
}