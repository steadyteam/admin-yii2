<?php

namespace Steady\Admin\Modules\Field;

use Steady\Admin\Components\SteadyModule;

class FieldModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'field',
        'class' => self::class,
        'title' => [
            'en' => 'Fields',
            'ru' => 'Поля',
        ],
        'icon' => 'unchecked',
        'order_num' => 80,
    ];
}