<?php

namespace Steady\Admin\Modules\Layout\Controllers;

use Steady\Admin\Components\TreeController;
use Steady\Engine\Modules\Layout\Models\LayoutModel;

class AController extends TreeController
{
    public $modelName = LayoutModel::class;

    public $moduleName = 'layout';

    public $linkRoute = '/a/edit';

    public $rootActions = ['create', 'delete'];
}