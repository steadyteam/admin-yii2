<?php

namespace Steady\Admin\Modules\Layout;

use Steady\Admin\Components\SteadyModule;

class LayoutModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'layout',
        'class' => self::class,
        'title' => [
            'en' => 'Layouts',
            'ru' => 'Шаблоны',
        ],
        'icon' => 'duplicate',
        'order_num' => 100,
    ];
}