<?php
return [
    'Layouts' => 'Шаблоны',
    'Create layout' => 'Создать шаблон',
    'Edit layout' => 'Редактировать шаблон',
    'Layout created' => 'Шаблон успешно создан',
    'Layout updated' => 'Шаблон обновлен',
    'Layout deleted' => 'Шаблон удален',
    'Parent layout' => 'Родительский шаблон',
];