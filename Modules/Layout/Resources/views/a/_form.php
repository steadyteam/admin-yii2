<?php

use Steady\Admin\Widgets\RedactorWidget;
use Steady\Engine\SW;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form'],
]); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'alias') ?>
<?= $form->field($model, 'parent_id')->dropDownList(
    ArrayHelper::map($validParents, 'layout_id', 'title'),
    ['prompt' => '- SelectWidget layout -']) ?>
<?= $form->field($model, 'type') ?>
<?= $form->field($model, 'route') ?>
<?= $form->field($model, 'content')->widget(RedactorWidget::class, [
    'options' => [
        'minHeight' => 400,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'layouts']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'layouts']),
        'plugins' => ['fullscreen'],
    ],
]) ?>

<?= $form->field($model, 'params')->textarea() ?>

<? /*= SeoFormWidget::widget(['model' => $model]) */ ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>