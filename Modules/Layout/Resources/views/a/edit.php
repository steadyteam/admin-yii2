<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/layout', 'Edit layout');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>