<?php

use Steady\Engine\SW;

$this->title = SW::t('admin/layout', 'Create layout');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>