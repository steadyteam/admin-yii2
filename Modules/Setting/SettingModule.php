<?php

namespace Steady\Admin\Modules\Setting;

use Steady\Admin\Components\SteadyModule;

class SettingModule extends SteadyModule
{
    public static $installConfig = [
        'name' => 'setting',
        'class' => self::class,
        'title' => [
            'en' => 'Settings',
            'ru' => 'Настройки',
        ],
        'icon' => 'cog',
        'order_num' => 60,
    ];
}