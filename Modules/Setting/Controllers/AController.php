<?php

namespace Steady\Admin\Modules\Setting\Controllers;

use Steady\Admin\Components\AdminController;
use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;

class AController extends AdminController
{
    public $rootActions = ['create', 'delete'];

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => SettingModel::find()->where([]),
        ]);
        SW::$app->user->setReturnUrl('/admin/setting');

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionCreate()
    {
        $model = new SettingModel;

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin', 'Setting created'));
                    return $this->redirect('/admin/setting');
                } else {
                    $this->flash('error', SW::t('admin', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = SettingModel::findOne($id);

        if ($model === null) {
            $this->flash('error', SW::t('admin', 'Not found'));
            return $this->redirect(['/admin/setting']);
        }

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', SW::t('admin', 'Setting updated'));
                } else {
                    $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        } else {
            return $this->render('edit', [
                'form' => [
                    'model' => $model,
                ],
            ]);
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = SettingModel::findOne($id);
        if ($model) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse(SW::t('admin', 'Setting deleted'));
    }
}