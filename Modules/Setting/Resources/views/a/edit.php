<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Edit setting');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>