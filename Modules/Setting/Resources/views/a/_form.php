<?php

use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin(['enableAjaxValidation' => true]); ?>
<?php if (IS_ROOT) : ?>
    <?= $form->field($model, 'alias')->textInput(!$model->isNewRecord ? ['disabled' => 'disabled'] : []) ?>
    <?= $form->field($model, 'hidden')->checkbox(['uncheck' => SettingModel::VISIBLE_ALL]) ?>
<?php endif ?>
<?= $form->field($model, 'title')->textarea(['disabled' => !IS_ROOT]) ?>
<?= $form->field($model, 'value')->textarea() ?>

<?= Html::submitButton(SW::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>