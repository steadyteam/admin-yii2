<?php

use Steady\Engine\SW;

$this->title = SW::t('admin', 'Create setting');

?>

<?= $this->render('_menu') ?>
<?= $this->render('_form', $form) ?>