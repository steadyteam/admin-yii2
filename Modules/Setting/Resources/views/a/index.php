<?php

use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\SW;
use yii\helpers\Url;

$this->title = SW::t('admin', 'Settings');
?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'Name') ?></th>
            <th><?= SW::t('admin', 'Title') ?></th>
            <th><?= SW::t('admin', 'Value') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $setting) : ?>
            <tr <?php if ($setting->hidden == SettingModel::VISIBLE_ROOT) echo 'class="warning"' ?>>
                <td><?= $setting->primaryKey ?></td>
                <td><?= $setting->alias ?></td>
                <td><a href="<?= Url::to(['/admin/setting/a/edit', 'id' => $setting->primaryKey]) ?>"
                       title="<?= SW::t('admin', 'Edit') ?>"><?= $setting->title ?></a>
                </td>
                <td style="overflow: hidden"><?= $setting->value ?></td>
                <td><a href="<?= Url::to(['/admin/setting/a/delete', 'id' => $setting->primaryKey]) ?>"
                       class="glyphicon glyphicon-remove confirm-delete"
                       title="<?= SW::t('admin', 'Delete item') ?>"></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $data->pagination,
        ]) ?>
    </table>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>