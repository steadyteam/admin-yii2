<?php

use Steady\Admin\Assets\AdminAsset;
use Steady\Engine\Helpers\Date;
use Steady\Engine\SW;
use yii\helpers\Html;
use yii\helpers\Url;

$asset = AdminAsset::register($this);
$moduleName = $this->context->module->id;

?>
<?php $this->beginPage() ?>
<html lang="<?= SW::$app->language ?>">
<head>
    <meta charset="<?= SW::$app->charset ?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= SW::t('admin', 'Control Panel') ?> - <?= Html::encode($this->title) ?></title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>
    <link rel="shortcut icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="admin-body">
    <div class="page-container">
        <div class="wrapper">
            <div class="header">
                <div class="logo">
                    <!--<img src="<? /*= $asset->baseUrl */ ?>/img/logo.png" width="20px" height="20px">-->
                    <a href="/admin" style="color: #fff; text-decoration: none;">SteadyWeb</a>
                </div>
                <div class="nav">
                    <a href="<?= SW::$app->homeUrl ?>" class="pull-left">
                        <i class="glyphicon glyphicon-home"></i>
                        <?= SW::t('admin', 'Open site') ?>
                    </a>
                    <a href="<?= Url::to(['/admin/sign/out']) ?>" class="pull-right">
                        <i class="glyphicon glyphicon-log-out"></i>
                        <?= SW::t('admin', 'Logout') ?>
                    </a>
                </div>
            </div>
            <div class="main">
                <div class="box sidebar">
                    <?php foreach (SW::$app->getModule('admin')->activeModules as $module) : ?>
                        <a href="<?= Url::to(["/admin/$module->name"]) ?>"
                           class="menu-item <?= ($moduleName == $module->name ? 'active' : '') ?>">
                            <?php if ($module->icon != '') : ?>
                                <i class="glyphicon glyphicon-<?= $module->icon ?>"></i>
                            <?php endif; ?>
                            <?php $moduleClass = $module->class; ?>
                            <?= $moduleClass::$installConfig['title'][Date::getLocale()]; ?>
                            <?php /*if ($module->notice > 0) : */ ?><!--
                                <span class="badge"><? /*= $module->notice */ ?></span>
                            --><?php /*endif; */ ?>
                        </a>
                    <?php endforeach; ?>
                    <!--<a href="<? /*= Url::to(['/admin/users']) */ ?>"
                       class="menu-item <? /*= ($moduleName == 'admin' && $this->context->id == 'users') ? 'active' : '' */ ?>">
                        <i class="glyphicon glyphicon-user"></i>
                        <? /*= SW::t('admin', 'Users') */ ?>
                    </a>
                    <a href="<? /*= Url::to(['/admin/settings']) */ ?>"
                       class="menu-item <? /*= ($moduleName == 'admin' && $this->context->id == 'settings') ? 'active' : '' */ ?>">
                        <i class="glyphicon glyphicon-cog"></i>
                        <? /*= SW::t('admin', 'Settings') */ ?>
                    </a>
                    <?php /*if (IS_ROOT) : */ ?>
                        <a href="<? /*= Url::to(['/admin/modules']) */ ?>"
                           class="menu-item <? /*= ($moduleName == 'admin' && $this->context->id == 'modules') ? 'active' : '' */ ?>">
                            <i class="glyphicon glyphicon-folder-close"></i>
                            <? /*= SW::t('admin', 'Modules') */ ?>
                        </a>
                        <a href="<? /*= Url::to(['/admin/system']) */ ?>"
                           class="menu-item <? /*= ($moduleName == 'admin' && $this->context->id == 'system') ? 'active' : '' */ ?>">
                            <i class="glyphicon glyphicon-hdd"></i>
                            <? /*= SW::t('admin', 'System') */ ?>
                        </a>
                        <a href="<? /*= Url::to(['/admin/logs']) */ ?>"
                           class="menu-item <? /*= ($moduleName == 'admin' && $this->context->id == 'logs') ? 'active' : '' */ ?>">
                            <i class="glyphicon glyphicon-align-justify"></i>
                            <? /*= SW::t('admin', 'Logs') */ ?>
                        </a>
                    --><?php /*endif; */ ?>
                </div>
                <div class="box content">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                    <div class="container-fluid">
                        <?php foreach (SW::$app->session->getAllFlashes() as $key => $message) : ?>
                            <div class="alert alert-<?= $key ?>"><?= $message ?></div>
                        <?php endforeach; ?>
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
