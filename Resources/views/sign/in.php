<?php

use Steady\Engine\SW;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$asset = \Steady\Admin\Assets\EmptyAsset::register($this);
$this->title = SW::t('admin', 'Sign in');
?>
<div class="container">
    <div id="wrapper" class="col-md-4 col-md-offset-4 vertical-align-parent">
        <div class="vertical-align-child">
            <div class="panel">
                <div class="panel-heading text-center">
                    <?= SW::t('admin', 'Sign in') ?>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'fieldConfig' => [
                            'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        ],
                    ])
                    ?>
                    <?= $form->field($model, 'username')
                        ->textInput(['class' => 'form-control', 'placeholder' => SW::t('admin', 'Username')]) ?>
                    <?= $form->field($model, 'password')
                        ->passwordInput(['class' => 'form-control', 'placeholder' => SW::t('admin', 'Password')]) ?>
                    <?= Html::submitButton(SW::t('admin', 'Login'),
                        ['class' => 'btn btn-lg btn-primary btn-block']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="text-center">
                <a class="logo" href="http://steadycms.com" target="_blank" title="SteadyWeb homepage">
                    <img src="<?= $asset->baseUrl ?>/img/logo_20.png">SteadyWeb
                </a>
            </div>
        </div>
    </div>
</div>
