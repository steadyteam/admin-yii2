<?php

use Steady\Admin\Components\SteadyModule;
use Steady\Modules\Catalog\Interfaces\Goodsable;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Helpers\Date;
use Steady\Engine\SW;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Url;

/**
 * @var TreeModel[] $tree
 * @var SteadyModule $moduleClass
 */

$moduleClass = get_class($this->context->module);
$this->title = $moduleClass::$installConfig['title'][Date::getLocale()];

$baseUrl = '/admin/' . $this->context->moduleName;
$linkUrl = $baseUrl . $this->context->linkRoute;
$linkController = $baseUrl . $this->context->linkController;

BootstrapPluginAsset::register($this);

?>

<?= $this->render($this->context->menuView) ?>

<?php if ($tree) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= SW::t('admin', 'Name') ?></th>
            <th><?= SW::t('admin', 'Tag') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tree as $node) : ?>
            <tr>
                <td><?= $node->primaryKey ?></td>
                <td style="padding-left: <?= $node->depth * 20 ?>px;">
                    <?php if (!$node->children()) : ?>
                        <!--<i class="caret"></i>-->
                    <?php endif; ?>
                    <?php if (!$node->children() || /*SW::$app->controller->module->settings['itemsInFolder'] ??*/
                        true) : ?>
                        <a href="<?= Url::to([$baseUrl . $this->context->linkRoute, 'id' => $node->primaryKey]) ?>"
                            <? /*= ($node->status == TreeModel::STATUS_OFF ? 'class="smooth"' : '') */ ?>>
                            <?= $node->title ?>
                        </a>
                    <?php else : ?>
                        <span <? /*= ($node->status == CategoryModel::STATUS_OFF ? 'class="smooth"' : '')*/ ?>>
                            <?= $node->title ?>
                        </span>
                    <?php endif; ?>
                    <?php if ($node instanceof Goodsable) : ?>
                        <span class="count"><?= $node->goodsCount() ?></span>
                    <?php endif; ?>
                </td>
                <td><?= $node->slug ?? $node->alias ?></td>
                <td width="120" class="text-right">
                    <?= $this->render('_actions', ['node' => $node, 'baseUrl' => $baseUrl, 'linkController' => $linkController]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <p><?= SW::t('admin', 'No records found') ?></p>
<?php endif; ?>