<?php

use Steady\Engine\Components\TreeModel;
use Steady\Engine\SW;
use yii\helpers\Url;

/**
 * @var TreeModel $node
 * @var string $linkController
 */

?>

<div class="dropdown actions">
    <i id="dropdownMenu<?= $node->primaryKey ?>" data-toggle="dropdown" aria-expanded="true"
       title="<?= SW::t('admin', 'Actions') ?>" class="glyphicon glyphicon-menu-hamburger"></i>
    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu<?= $node->primaryKey ?>">
        <li>
            <a href="<?= Url::to(["$linkController/edit", 'id' => $node->primaryKey]) ?>">
                <i class="glyphicon glyphicon-pencil font-12"></i><?= SW::t('admin', 'Edit') ?>
            </a>
        </li>
        <li>
            <a href="<?= Url::to(["$linkController/create", 'parent_id' => $node->primaryKey]) ?>">
                <i class="glyphicon glyphicon-plus font-12"></i><?= SW::t('admin', 'Add subcategory') ?>
            </a>
        </li>
        <li role="presentation" class="divider"></li>
        <li>
            <a href="<?= Url::to(["$linkController/up", 'id' => $node->primaryKey]) ?>">
                <i class="glyphicon glyphicon-arrow-up font-12"></i><?= SW::t('admin', 'Move up') ?>
            </a>
        </li>
        <li>
            <a href="<?= Url::to(["$linkController/down", 'id' => $node->primaryKey]) ?>">
                <i class="glyphicon glyphicon-arrow-down font-12"></i><?= SW::t('admin', 'Move down') ?>
            </a>
        </li>
        <li role="presentation" class="divider"></li>
        <?php /*if ($node->status == TreeModel::STATUS_ON) : */ ?><!--
        <li>
            <a href="<? /*= Url::to(["$linkController/off', 'id' => $node->primaryKey]) */ ?>"
               title="<? /*= Steady::t('admin', 'Turn Off') */ ?>'">
                <i class="glyphicon glyphicon-eye-close font-12"></i><? /*= Steady::t('admin', 'Turn Off') */ ?>
            </a>
        </li>
    <?php /*else : */ ?>
        <li>
            <a href="<? /*= Url::to(["$linkController/on', 'id' => $node->primaryKey]) */ ?>"
               title="<? /*= Steady::t('admin', 'Turn On') */ ?>">
                <i class="glyphicon glyphicon-eye-open font-12"></i><? /*= Steady::t('admin', 'Turn On') */ ?>
            </a>
        </li>
    --><?php /*endif; */ ?>
        <li>
            <a href="<?= Url::to(["$linkController/delete", 'id' => $node->primaryKey]) ?>"
               class="confirm-delete" data-reload="1" title="<?= SW::t('admin', 'Delete item') ?>">
                <i class="glyphicon glyphicon-remove font-12"></i><?= SW::t('admin', 'Delete') ?>
            </a>
        </li>
    </ul>
</div>
