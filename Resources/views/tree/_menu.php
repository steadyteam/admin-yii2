<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;
?>
<div class="action-list">
    <?php if ($action != 'index') : ?>
        <a class="btn btn-primary" href="<?= $this->context->getReturnUrl(["/admin/$module"]) ?>">
            <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?= SW::t('admin', 'Back') ?>
        </a>
    <?php else : ?>
        <a class="btn btn-success" href="<?= Url::to(["/admin/$module/a/create"]) ?>">
            <i class="glyphicon glyphicon-plus font-12"></i>
            <?= SW::t('admin', 'Create') ?>
        </a>
    <?php endif; ?>
</div>
<br/>
