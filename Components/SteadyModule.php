<?php

namespace Steady\Admin\Components;

use Steady\Engine\Base\Module;
use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Engine\SW;

class SteadyModule extends Module
{
    /**
     * @var array
     */
    public static $installConfig = [
        'name' => 'custom',
        'class' => self::class,
        'title' => [
            'en' => 'Custom Module',
        ],
        'icon' => 'asterisk',
        'order_num' => 0,
    ];
    /**
     * @var string
     */
    public $moduleName;
    /**
     * @var string
     */
    public $defaultRoute = 'a';
    /**
     * @var array
     */
    public $settings = [];
    /**
     * @var @todo
     */
    public $i18n;

    /**
     * @throws \ReflectionException
     */
    public function init()
    {
        parent::init();

        $this->moduleName = self::getModuleName(static::class);
        $moduleDir = dirname((new \ReflectionClass($this))->getFileName());

        $configFile = "$moduleDir/Config/module.php";
        //SW::getAlias("@admin/modules/$this->moduleName/Config/module.php");
        if (file_exists($configFile)) {
            SW::configure($this, require $configFile);
        }

        self::registerTranslations($this->moduleName);
    }

    /**
     * Registers translations connected to the module
     * @name $moduleName string
     * @throws \ReflectionException
     */
    public static function registerTranslations($moduleName)
    {
        $moduleClassFile = '';
        foreach (ModuleModel::findAllActive() as $name => $module) {
            if ($name == $moduleName) {
                $moduleClassFile = (new \ReflectionClass($module->class))->getFileName();
                break;
            }
        }

        if ($moduleClassFile) {
            SW::$app->i18n->translations['admin/' . $moduleName] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => dirname($moduleClassFile) . DIRECTORY_SEPARATOR . 'Resources/messages',
                'fileMap' => [
                    'admin/' . $moduleName => 'admin.php',
                    'admin/' . $moduleName . '/api' => 'api.php',
                ],
            ];
        }

        //print_r(SW::$app->i18n->translations);
    }

    /**
     * Module name getter
     * @name $namespace
     * @return string|bool
     */
    public static function getModuleName($namespace)
    {
        foreach (ModuleModel::findAllActive() as $module) {
            $moduleClassPath = preg_replace('/[\w]+$/', '', $module->class);
            if (strpos($namespace, $moduleClassPath) !== false) {
                return $module->name;
            }
        }
        return false;
    }
}