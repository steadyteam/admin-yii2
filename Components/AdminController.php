<?php

namespace Steady\Admin\Components;

use Steady\Engine\Base\Controller;
use Steady\Engine\SW;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

abstract class AdminController extends Controller
{
    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @var array
     */
    public $rootActions = [];

    /**
     * @var null
     */
    public $error = null;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->layout = SW::$app->getModule('admin')->controllerLayout;
    }

    /**
     * Check authentication, and root rights for actions
     *
     * @name \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        //return true;
        // TODO: Разработать новую систему контроля доступа.
        if (SW::$app->user->isGuest) {
            SW::$app->user->setReturnUrl(SW::$app->request->url);
            SW::$app->getResponse()->redirect(['/admin/sign/in'])->send();
            return false;
        } else {
            if (!IS_ADMIN) {
                throw new ForbiddenHttpException('Access denied!');
            }
            if (!IS_ROOT && ($this->rootActions == 'all' || in_array($action->id, $this->rootActions))) {
                throw new ForbiddenHttpException('You cannot access this action');
            }
            if ($action->id === 'index') {
                $this->setReturnUrl();
            }

            return true;
        }
    }

    /**
     * Set return url for module in sessions
     * @name mixed $url if not set, returnUrl will be current page
     */
    public function setReturnUrl($url = null)
    {
        SW::$app->getSession()->set($this->module->id . '_return', $url ? Url::to($url) : Url::current());
    }

    /**
     * Get return url for module from session
     * @name mixed $defaultUrl if return url not found in sessions
     * @return mixed
     */
    public function getReturnUrl($defaultUrl = null)
    {
        return SW::$app->getSession()->get($this->module->id . '_return', $defaultUrl
            ? Url::to($defaultUrl)
            : Url::to('/admin/' . $this->module->id));
    }

    /**
     * Formats response depending on request type (ajax or not)
     * @name string $success
     * @name bool $back go back or refresh
     * @return mixed $result array if request is ajax.
     */
    public function formatResponse($success = '', $back = true)
    {
        if (SW::$app->request->isAjax) {
            SW::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($this->error) {
                return ['result' => 'error', 'error' => $this->error];
            } else {
                $response = ['result' => 'success'];
                if ($success) {
                    if (is_array($success)) {
                        $response = array_merge(['result' => 'success'], $success);
                    } else {
                        $response = array_merge(['result' => 'success'], ['message' => $success]);
                    }
                }
                return $response;
            }
        } else {
            if ($this->error) {
                $this->flash('error', $this->error);
            } else {
                if (is_array($success) && isset($success['message'])) {
                    $this->flash('success', $success['message']);
                } else if (is_string($success)) {
                    $this->flash('success', $success);
                }
            }
            return $back ? $this->back() : $this->refresh();
        }
    }

    /**
     * Write in sessions alert messages
     * @name string $type error or success
     * @name string $message alert body
     */
    public function flash($type, $message)
    {
        SW::$app->getSession()->setFlash($type == 'error' ? 'danger' : $type, $message);
    }

    /**
     * @return Response
     */
    public function back(): Response
    {
        return $this->redirect(SW::$app->request->referrer);
    }
}
