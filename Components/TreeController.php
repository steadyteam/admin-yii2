<?php

namespace Steady\Admin\Components;

use Steady\Engine\Components\Task;
use Steady\Engine\Components\TreeModel;
use Steady\Engine\Helpers\Image;
use Steady\Engine\Modules\Field\Behaviors\FieldableBehavior;
use Steady\Engine\SW;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\Response;
use yii\web\UploadedFile;

abstract class TreeController extends AdminController
{
    /**
     * @var string
     */
    public $modelName;

    /**
     * @var string
     */
    public $moduleName;

    /**
     * @var string
     */
    public $linkController = '/a';

    /**
     * @var string
     */
    public $linkRoute = '/items';

    /**
     * @var string
     */
    public $menuView = '_menu';

    /**
     * @name TreeModel $model
     * @name string $message
     * @return Response
     */
    protected function save($model, $message = 'create')
    {
        if ($message == 'create') {
            $msgSuccess = ucfirst($this->moduleName) . ' created';
            $msgError = 'Create error. {0}';
        } else if ($message == 'edit') {
            $msgSuccess = ucfirst($this->moduleName) . ' updated';
            $msgError = 'Update error. {0}';
        } else {
            $msgSuccess = ucfirst($this->moduleName) . ' saved';
            $msgError = 'Save error. {0}';
        }

        if ($model->save()) {
            $this->flash('success', SW::t("admin/$this->moduleName", $msgSuccess));
            return $this->redirect($this->getRedirect());
        } else {
            $this->flash('error', SW::t('admin', $msgError, $model->formatErrors()));
            return $this->refresh();
        }
    }

    /**
     * @name TreeModel $model
     */
    protected function beforeSave($model)
    {
        $modelClass = $this->getClass();
    }

    /**
     * Tree
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $modelClass = $this->getClass();

        return $this->render('@admin/Resources/views/tree/index', [
            'tree' => $modelClass::tree(),
        ]);
    }

    /**
     * Create form
     *
     * @name null $parent_id
     * @name null $slug
     * @return array|string|Response|Task
     * @throws \yii\db\Exception
     */
    public function actionCreate($parent_id = null, $slug = null)
    {
        $modelClass = $this->getClass();
        $model = $this->getModel();

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                return $this->ajaxValidate($model);
            } else {
                $this->beforeSave($model);
                return $this->save($model);
            }
        } else {
            if ($parent_id) $model->parent_id = $parent_id;
            if ($slug) $model->slug = $slug;

            $validParents = $modelClass::tree(true);
            $params = [
                'form' => [
                    'model' => $model,
                    'validParents' => $validParents,
                ],
            ];

            $task = $this->directive(self::TASK_CUSTOM_RENDER);
            if ($task) {
                return $task->setData($params)->realization();
            }

            return $this->render('create', $params);
        }
    }

    /**
     * Edit form
     *
     * @name $id
     * @return array|string|Response
     * @throws \yii\db\Exception
     */
    public function actionEdit($id)
    {
        $modelClass = $this->getClass();
        $model = $modelClass::findOne($id);

        if (!$model) {
            return $this->redirect($this->getRedirect());
        }

        if ($model->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                return $this->ajaxValidate($model);
            } else {
                $this->beforeSave($model);
                return $this->save($model, 'edit');
            }
        } else {
            $params = [
                'form' => [
                    'model' => $model,
                    'validParents' => $model->validParents(),
                ],
            ];

            $task = $this->directive(self::TASK_CUSTOM_RENDER);
            if ($task) {
                return $task->setData($params)->realization();
            }

            return $this->render('edit', $params);
        }
    }

    /**
     * @name $id
     * @name $alias
     * @name $type
     * @name bool $required
     * @name bool $folder
     * @return bool|Response
     * @throws \yii\db\Exception
     */
    public function actionField($id, $alias, $type, $required = false, $folder = false)
    {
        $modelClass = $this->getClass();
        /** @var FieldableBehavior $model */
        $model = $modelClass::findOne($id);

        if (!$model) {
            return $this->redirect($this->getRedirect());
        }

        return (bool)$model->addField($alias, $type, $required, $folder);
    }

    /**
     * @name $id
     * @name $alias
     * @name $content
     * @return bool|Response
     * @throws \yii\db\Exception
     */
    public function actionSetField($id, $alias, $content)
    {
        $modelClass = $this->getClass();
        /** @var FieldableBehavior $model */
        $model = $modelClass::findOne($id);

        if (!$model) {
            return $this->redirect($this->getRedirect());
        }

        return (bool)$model->setField($alias, $content);
    }

    /**
     * Remove category image
     *
     * @name $id
     * @return Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionClearImage($id)
    {
        $modelClass = $this->getClass();
        $model = $modelClass::findOne($id);

        if (!$model) {
            return $this->redirect($this->getRedirect());
        }

        if (isset($model->image)) {
            $model->image = '';
            if ($model->update()) {
                $this->flash('success', SW::t('admin', 'Image cleared'));
            } else {
                $this->flash('error', SW::t('admin', 'Update error. {0}', $model->formatErrors()));
            }
        }
        return $this->back();
    }

    /**
     * Delete the category by ID
     *
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionDelete($id)
    {
        $modelClass = $this->getClass();
        $model = $modelClass::findOne($id);

        if ($model) {
            $children = $model->children();
            $model->deleteWithChildren();
            foreach ($children as $child) {
                $child->afterDelete();
            }
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse(SW::t('admin', ucfirst($this->moduleName) . ' deleted'));
    }

    /**
     * Move category one level up
     *
     * @name $id
     * @return Response
     */
    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    /**
     * Move category one level down
     *
     * @name $id
     * @return Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    /**
     * Activate category action
     *
     * @name $id
     * @return mixed
     */
    public function actionOn($id)
    {
        $modelClass = $this->getClass();
        return $this->changeStatus($id, $modelClass::STATUS_ON);
    }

    /**
     * Activate category action
     *
     * @name $id
     * @return mixed
     */
    public function actionOff($id)
    {
        $modelClass = $this->getClass();
        return $this->changeStatus($id, $modelClass::STATUS_OFF);
    }

    /**
     * @throws \Exception
     */
    public function actionUpdateTree()
    {
        $modelClass = $this->getClass();
        $modelClass::buildTree();

        return 'Success!';
    }

    /**
     * Move category up/down
     *
     * @name $id
     * @name $direction
     * @return Response
     */
    protected function move($id, $direction)
    {
        //return $this->back();
        $modelClass = $this->getClass();
        $model = $modelClass::findOne($id);

        if ($model) {
            if ($direction == 'up') {
                $model->up();
            } else {
                $model->down();
            }
        } else {
            $this->flash('error', SW::t('admin', 'Not found'));
        }
        return $this->back();
    }

    /**
     * Change category status
     *
     * @name $id
     * @name $status
     * @return mixed
     */
    protected function changeStatus($id, $status)
    {
        $modelClass = $this->getClass();
        $model = $modelClass::findOne($id);

        if ($model) {
            $ids = [];
            $ids[] = $model->primaryKey;
            foreach ($model->children()->all() as $child) {
                $ids[] = $child->primaryKey;
            }
            $modelClass::updateAll(['status' => $status], ['in', 'category_id', $ids]);
            $model->trigger(ActiveRecord::EVENT_AFTER_UPDATE);
        } else {
            $this->error = SW::t('admin', 'Not found');
        }

        return $this->formatResponse(SW::t('admin', 'Status successfully changed'));
    }

    /**
     * @name Model $model
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    protected function saveImage(Model $model)
    {
        $model->image = UploadedFile::getInstance($model, 'image');
        if ($model->image && $model->validate(['image'])) {
            $model->image = Image::upload($model->image, $this->moduleName);
        } else {
            if ($model->isNewRecord) {
                $model->image = '';
            } else {
                $model->image = $model->oldAttributes['image'];
            }
        }
    }

    /**
     * @return array
     */
    protected function getRedirect()
    {
        return ["/admin/$this->moduleName"];
    }

    /**
     * @return TreeModel
     */
    protected function getModel()
    {
        $modelClass = self::getClass();

        $model = new $modelClass;
        return $model;
    }

    /**
     * @return TreeModel
     */
    protected function getClass()
    {
        /** @var TreeModel $modelClass */
        $modelClass = $this->modelName;
        return $modelClass;
    }
}