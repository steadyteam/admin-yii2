<?php

namespace Steady\Admin\Assets;

use yii\web\AssetBundle;

class FrontendAsset extends AssetBundle
{
    public $sourcePath = '@admin-frontend';
    public $css = [
        'css/frontend.css',
    ];
    public $js = [
        'js/frontend.js',
    ];
    public $depends = [
    ];
}
