<?php

namespace Steady\Admin\Assets;

use yii\web\AssetBundle;
use yii\web\View;

class AdminAsset extends AssetBundle
{
    public $sourcePath = '@admin-frontend';

    public $css = [
        'css/admin.css',
    ];
    public $js = [
        'js/admin.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'Steady\Admin\Assets\SwitcherAsset',
    ];
    public $jsOptions = array(
        'position' => View::POS_HEAD,
    );
}
