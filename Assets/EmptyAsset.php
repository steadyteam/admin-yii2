<?php

namespace Steady\Admin\Assets;

use yii\web\AssetBundle;

class EmptyAsset extends AssetBundle
{
    public $sourcePath = '@admin-frontend';
    public $css = [
        'css/empty.css',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
