<?php

namespace Steady\Admin\Assets;

use yii\web\AssetBundle;

class PhotosAsset extends AssetBundle
{
    public $sourcePath = '@admin-frontend/libs/photos';

    public $css = [
        'photos.css',
    ];

    public $js = [
        'photos.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
