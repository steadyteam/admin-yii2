<?php

namespace Steady\Admin\Behaviors;

use Steady\Engine\SW;
use yii\base\Behavior;

/**
 * Status behavior. Adds statuses to models
 * @package Steady\Admin\Behaviors
 */
class StatusControllerBehavior extends Behavior
{
    public $model;

    public function changeStatus($id, $status)
    {
        $modelClass = $this->model;

        if (($model = $modelClass::findOne($id))) {
            $model->status = $status;
            $model->update();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }

        return $this->owner->formatResponse(SW::t('admin', 'Status successfully changed'));
    }
}