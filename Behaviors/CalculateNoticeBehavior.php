<?php

namespace Steady\Admin\Behaviors;

use Steady\Admin\Components\SteadyModule;
use Steady\Engine\Modules\Module\Models\ModuleModel;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class CalculateNoticeBehavior extends Behavior
{
    public $callback;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'updateNotice',
            ActiveRecord::EVENT_AFTER_UPDATE => 'updateNotice',
            ActiveRecord::EVENT_AFTER_DELETE => 'updateNotice',
        ];
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateNotice()
    {
        $moduleName = SteadyModule::getModuleName(get_class($this->owner));
        if (($module = ModuleModel::findOne(['name' => $moduleName]))) {
            $module->notice = call_user_func($this->callback);
            $module->update();
        }
    }
}