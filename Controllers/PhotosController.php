<?php

namespace Steady\Admin\Controllers;

use Steady\Admin\Behaviors\SortableControllerBehavior;
use Steady\Admin\Components\AdminController;
use Steady\Engine\Helpers\Image;
use Steady\Engine\Models\PhotoModel;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\GoodsModel;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * @mixin ContentNegotiator
 * @mixin SortableControllerBehavior
 * @mixin PhotoModel
 */
class PhotosController extends AdminController
{
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => SortableControllerBehavior::class,
                'model' => PhotoModel::class,
            ],
        ];
    }

    /**
     * @name $class_alias
     * @name $owner_id
     * @return mixed
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function actionUpload($owner_id, $class_alias)
    {
        $success = null;

        if ($_FILES['Photo']) {
            $_FILES['PhotoModel'] = $_FILES['Photo'];
        }

        $photo = new PhotoModel();
        $photo->owner_id = $owner_id;
        $photo->class_alias = $class_alias;
        $photo->image = UploadedFile::getInstance($photo, 'image');

        if ($photo->image && $photo->validate(['image'])) {
            $photo->image = Image::upload($photo->image, 'photos', PhotoModel::PHOTO_MAX_WIDTH);
            $watermark = '@frontend/img/watermark.png';//ModuleModel::findAllActive()['catalog']->settings['watermark'];
            if ($photo->class_alias == GoodsModel::getClassAlias() && $watermark) {
                Image::watermark($photo->image, $watermark);
            }

            if ($photo->image) {
                if ($photo->save()) {
                    $success = [
                        'message' => SW::t('admin', 'Photo uploaded'),
                        'photo' => [
                            'id' => $photo->primaryKey,
                            'image' => $photo->image,
                            'thumb' => Image::thumb($photo->image, PhotoModel::PHOTO_THUMB_WIDTH,
                                PhotoModel::PHOTO_THUMB_HEIGHT),
                            'description' => '',
                        ],
                    ];
                } else {
                    @unlink(SW::getAlias('@root') . str_replace(Url::base(true), '', $photo->image));
                    $this->error = SW::t('admin', 'Create error. {0}', $photo->formatErrors());
                }
            } else {
                $this->error = SW::t('admin', 'File upload error. Check uploads folder for write permissions');
            }
        } else {
            $this->error = SW::t('admin', 'File is incorrect');
        }

        return $this->formatResponse($success);
    }

    /**
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDescription($id)
    {
        if (($model = PhotoModel::findOne($id))) {
            if (SW::$app->request->post('description')) {
                $model->description = SW::$app->request->post('description');
                if (!$model->update()) {
                    $this->error = SW::t('admin', 'Update error. {0}', $model->formatErrors());
                }
            } else {
                $this->error = SW::t('admin', 'Bad response');
            }
        } else {
            $this->error = SW::t('admin', 'Not found');
        }

        return $this->formatResponse(SW::t('admin', 'Photo description saved'));
    }

    /**
     * @name $id
     * @return mixed
     * @throws \ImagickException
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     */
    public function actionImage($id)
    {
        $success = null;

        if ($_FILES['Photo']) {
            $_FILES['PhotoModel'] = $_FILES['Photo'];
        }

        /** @var PhotoModel $photo */
        $photo = PhotoModel::findOne($id);

        if ($photo) {
            $oldImage = $photo->image;

            $photo->image = UploadedFile::getInstance($photo, 'image');

            if ($photo->image && $photo->validate(['image'])) {
                $photo->image = Image::upload($photo->image, 'photos', PhotoModel::PHOTO_MAX_WIDTH);
                $watermark = '@frontend/img/watermark.png';//ModuleModel::findAllActive()['catalog']->settings['watermark'];
                if ($photo->class_alias == GoodsModel::getClassAlias() && $watermark) {
                    Image::watermark($photo->image, $watermark);
                }
                if ($photo->image) {
                    if ($photo->save()) {
                        @unlink(SW::getAlias('@root') . $oldImage);

                        $success = [
                            'message' => SW::t('admin', 'Photo uploaded'),
                            'photo' => [
                                'image' => $photo->image,
                                'thumb' => Image::thumb($photo->image, PhotoModel::PHOTO_THUMB_WIDTH,
                                    PhotoModel::PHOTO_THUMB_HEIGHT),
                            ],
                        ];
                    } else {
                        @unlink(SW::getAlias('@root') . $photo->image);

                        $this->error = SW::t('admin', 'Update error. {0}', $photo->formatErrors());
                    }
                } else {
                    $this->error = SW::t('admin', 'File upload error. Check uploads folder for write permissions');
                }
            } else {
                $this->error = SW::t('admin', 'File is incorrect');
            }

        } else {
            $this->error = SW::t('admin', 'Not found');
        }

        return $this->formatResponse($success);
    }

    /**
     * @name $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if (($model = PhotoModel::findOne($id))) {
            $model->delete();
        } else {
            $this->error = SW::t('admin', 'Not found');
        }
        return $this->formatResponse(SW::t('admin', 'Photo deleted'));
    }

    /**
     * @name $id
     * @name $owner_id
     * @name $class_alias
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionUp($id, $owner_id, $class_alias)
    {
        return $this->move($id, 'up', ['owner_id' => $owner_id, 'class_alias' => $class_alias]);
    }

    /**
     * @name $id
     * @name $owner_id
     * @name $class_alias
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDown($id, $owner_id, $class_alias)
    {
        return $this->move($id, 'down', ['owner_id' => $owner_id, 'class_alias' => $class_alias]);
    }
}