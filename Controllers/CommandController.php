<?php

namespace Steady\Admin\Controllers;

use Steady\Admin\Components\AdminController;
use Steady\Engine\Helpers\WebConsole;
use Steady\Engine\SW;

class CommandController extends AdminController
{
    /**
     * @name $command
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     * @throws \yii\web\ServerErrorHttpException
     */
    public function actionRun(string $command)
    {
        $webConsole = $this->getWebConsole();

        return $webConsole->execute($command);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     * @throws \yii\web\ServerErrorHttpException
     */
    public function actionMigrateUp()
    {
        $webConsole = $this->getWebConsole();

        return $webConsole->migrate();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\console\Exception
     * @throws \yii\web\ServerErrorHttpException
     */
    public function actionMigrateDown()
    {
        $webConsole = $this->getWebConsole();

        return $webConsole->migrateDown();
    }

    /**
     * @return WebConsole
     */
    protected function getWebConsole()
    {
        return new WebConsole(SW::getAlias('@backend/Config/console.php'));
    }
}