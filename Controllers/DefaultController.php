<?php

namespace Steady\Admin\Controllers;

use Steady\Admin\Components\AdminController;

class DefaultController extends AdminController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}