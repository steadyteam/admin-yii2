<?php

namespace Steady\Admin\Controllers;

use Steady\Engine\Base\Controller;
use Steady\Engine\Modules\User\Models\AuthModel;
use Steady\Engine\SW;

class SignController extends Controller
{
    public $layout = 'empty';

    public $enableCsrfValidation = false;

    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionIn()
    {
        $model = new AuthModel();

        if (!SW::$app->user->isGuest || ($model->load(SW::$app->request->post()) && $model->login())) {
            return $this->redirect(SW::$app->user->getReturnUrl(['/admin']));
        } else {
            return $this->render('in', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionOut()
    {
        SW::$app->user->logout();

        return $this->redirect(SW::$app->homeUrl);
    }
}