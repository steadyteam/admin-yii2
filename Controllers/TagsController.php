<?php

namespace Steady\Admin\Controllers;

use Steady\Admin\Components\AdminController;
use Steady\Engine\Models\TagModel;
use Steady\Engine\SW;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use yii\web\Response;

class TagsController extends AdminController
{
    public function actionList($query)
    {
        SW::$app->response->format = Response::FORMAT_JSON;

        $items = [];
        $query = urldecode(mb_convert_encoding($query, "UTF-8"));

        $tags = TagModel::find()
            ->where(['like', 'name', $query])
            ->asArray()
            ->all();

        foreach ($tags as $tag) {
            $items[] = ['name' => $tag['name']];
        }

        return $items;
    }

    public function actionBrands($depth = null, $query)
    {
        SW::$app->response->format = Response::FORMAT_JSON;

        $items = [];
        $query = urldecode(mb_convert_encoding($query, "UTF-8"));

        $brands = BrandModel::find()
            ->where(['like', 'title', $query])
            ->andWhere(['>=', 'depth', $depth ?? 0])
            ->asArray()
            ->all();

        foreach ($brands as $brand) {
            $items[] = ['name' => $brand['title']];
        }

        return $items;
    }

    public function actionCategories($id, $query)
    {
        SW::$app->response->format = Response::FORMAT_JSON;

        $items = [];
        $query = urldecode(mb_convert_encoding($query, "UTF-8"));

        $categories = CategoryModel::find()
            ->where(['like', 'title', $query])
            ->andWhere(['parent_id' => $id])
            ->asArray()
            ->all();

        foreach ($categories as $category) {
            $items[] = ['name' => $category['title']];
        }

        return $items;
    }
}