<?php

namespace Steady\Admin\Widgets;

use Steady\Engine\Base\Model;
use Steady\Engine\Models\PhotoModel;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class PhotosWidget extends Widget
{
    /**
     * @var Model
     */
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->model)) {
            throw new InvalidConfigException('Required `model` name isn\'t set.');
        }
    }

    /**
     * @return string|void
     */
    public function run()
    {
        $photos = PhotoModel::find()->where(['class_alias' => $this->model->getAlias(), 'owner_id' => $this->model->primaryKey])
            ->all();

        echo $this->render('photos', [
            'photos' => $photos,
        ]);
    }

}