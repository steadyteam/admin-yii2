<?php

namespace Steady\Admin\Widgets;

use Steady\Admin\Assets\FancyboxAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Json;

class FancyboxWidget extends Widget
{
    public $options = [];
    public $selector;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->selector)) {
            throw new InvalidConfigException('Required `selector` name isn\'t set.');
        }
    }

    /**
     * @return string|void
     * @throws InvalidConfigException
     */
    public function run()
    {
        $clientOptions = (count($this->options)) ? Json::encode($this->options) : '';

        $this->view->registerAssetBundle(FancyboxAsset::className());
        $this->view->registerJs('$("' . $this->selector . '").fancybox(' . $clientOptions . ');');
    }
}