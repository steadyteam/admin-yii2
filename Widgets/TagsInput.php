<?php

namespace Steady\Admin\Widgets;

use dosamigos\selectize\SelectizeTextInput;

/**
 * TODO: Добавить возможность конфигурировать из config приложения, желательно для всех
 */
class TagsInput extends SelectizeTextInput
{
    public $options = ['class' => 'form-control'];

    public $loadUrl = ['/admin/tags/list'];

    public $clientOptions = [
        'plugins' => [
            'remove_button',
        ],
        'valueField' => 'name',
        'labelField' => 'name',
        'searchField' => [
            'name',
        ],
        'create' => true,
    ];
}