<?php

use Steady\Admin\Assets\PhotosAsset;
use Steady\Admin\Widgets\FancyboxWidget;
use Steady\Engine\Helpers\Image;
use Steady\Engine\Models\PhotoModel;
use Steady\Engine\SW;
use yii\helpers\Html;
use yii\helpers\Url;

PhotosAsset::register($this);
FancyboxWidget::widget(['selector' => '.plugin-box']);

$owner_id = $this->context->model->primaryKey;
$class_alias = $this->context->model->getAlias();

$linkParams = [
    'owner_id' => $owner_id,
    'class_alias' => $class_alias,
];

$photoTemplate = '<tr data-id="{{photo_id}}">' . (IS_ROOT ? '<td>{{photo_id}}</td>' : '') . '\
    <td><a href="{{photo_image}}" class="plugin-box" title="{{photo_description}}" rel="steady-photos"><img class="photo-thumb" id="photo-{{photo_id}}" src="{{photo_thumb}}"></a></td>\
    <td>\
        <textarea class="form-control photo-description">{{photo_description}}</textarea>\
        <a href="' . Url::to(['/admin/photos/description/{{photo_id}}']) . '" class="btn btn-sm btn-primary disabled save-photo-description">' . SW::t('admin', 'Save') . '</a>\
    </td>\
    <td class="control vtop">\
        <div class="btn-group btn-group-sm" role="group">\
            <a href="' . Url::to(['/admin/photos/up/{{photo_id}}'] + $linkParams) . '" class="btn btn-default move-up" title="' . SW::t('admin', 'Move up') . '"><span class="glyphicon glyphicon-arrow-up"></span></a>\
            <a href="' . Url::to(['/admin/photos/down/{{photo_id}}'] + $linkParams) . '" class="btn btn-default move-down" title="' . SW::t('admin', 'Move down') . '"><span class="glyphicon glyphicon-arrow-down"></span></a>\
            <a href="' . Url::to(['/admin/photos/image/{{photo_id}}'] + $linkParams) . '" class="btn btn-default change-image-button" title="' . SW::t('admin', 'Change image') . '"><span class="glyphicon glyphicon-floppy-disk"></span></a>\
            <a href="' . Url::to(['/admin/photos/delete/{{photo_id}}']) . '" class="btn btn-default color-red delete-photo" title="' . SW::t('admin', 'Delete item') . '"><span class="glyphicon glyphicon-remove"></span></a>\
            <input type="file" name="PhotoModel[image]" class="change-image-input hidden">\
        </div>\
    </td>\
</tr>';
$this->registerJs("
var photoTemplate = '{$photoTemplate}';
", \yii\web\View::POS_HEAD);
$photoTemplate = str_replace('>\\', '>', $photoTemplate);

?>

    <button id="photo-upload" class="btn btn-success text-uppercase">
        <span class="glyphicon glyphicon-arrow-up"></span>
        <?= SW::t('admin', 'Upload') ?>
    </button>
    <small id="uploading-text" class="smooth">
        <?= SW::t('admin', 'Uploading. Please wait') ?>
        <span></span>
    </small>

    <table id="photo-table" class="table table-hover" style="display: <?= count($photos) ? 'table' : 'none' ?>;">
        <thead>
        <tr>
            <th width="50">#</th>
            <th width="150"><?= SW::t('admin', 'Image') ?></th>
            <th><?= SW::t('admin', 'Description') ?></th>
            <th width="150"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($photos as $photo) : ?>
            <?= str_replace(
                ['{{photo_id}}', '{{photo_thumb}}', '{{photo_image}}', '{{photo_description}}'],
                [$photo->primaryKey, Image::thumb($photo->image, PhotoModel::PHOTO_THUMB_WIDTH, PhotoModel::PHOTO_THUMB_HEIGHT),
                    $photo->image, $photo->description],
                $photoTemplate)
            ?>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p class="empty" style="display: <?= count($photos) ? 'none' : 'block' ?>;">
        <?= SW::t('admin', 'No photos uploaded yet') ?>
    </p>

<?= Html::beginForm(Url::to(['/admin/photos/upload'] + $linkParams), 'post',
    ['enctype' => 'multipart/form-data']) ?>
<?= Html::fileInput('PhotoModel[image]', null, [
    'id' => 'photo-file',
    'class' => 'hidden',
    'multiple' => 'multiple',
])
?>
<?php Html::endForm() ?>