<?php

namespace Steady\Admin\Widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;

class SeoFormWidget extends Widget
{
    public $model;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->model)) {
            throw new InvalidConfigException('Required `model` name isn\'t set.');
        }
    }

    public function run()
    {
        echo $this->render('seo_form', [
            'model' => $this->model->seoText,
        ]);
    }

}