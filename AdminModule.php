<?php

namespace Steady\Admin;

use Steady\Engine\Base\Module;
use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Engine\SW;
use yii\web\ServerErrorHttpException;

class AdminModule extends Module
{
    const VERSION = 0.9;

    public $settings;

    public $activeModules;

    public $controllerLayout = '@admin/Resources/views/layouts/main';

    /**
     * @throws ServerErrorHttpException
     */
    public function init()
    {
        parent::init();

        if (SW::$app->cache === null) {
            throw new ServerErrorHttpException('Please configure Cache component.');
        }

        $this->activeModules = ModuleModel::findAllActive();

        $modules = [];

        // TODO: Добавленно для тестирования, убрать в дальнейшем
        //SW::$app->cache->flush();

        foreach ($this->activeModules as $name => $module) {
            $modules[$name]['class'] = $module->class;
            if (is_array($module->settings)) {
                $modules[$name]['settings'] = $module->settings;
            }
        }
        $this->setModules($modules);

        if (SW::$app instanceof \yii\web\Application) {
            // TODO: Доработать систему авторизации
            define('IS_ROOT', !SW::$app->user->isGuest && SW::$app->user->identity->isRoot());
            define('IS_ADMIN', !SW::$app->user->isGuest && SW::$app->user->identity->isAdmin());
            // TODO: Добавить систему LiveEdit
            //define('LIVE_EDIT', false /*!SW::$app->user->isGuest && SW::$app->session->get('steady_live_edit')*/);
        }
    }
}
