<?php

namespace Steady\Admin\Helpers;

use Steady\Admin\Widgets\TagsInput;
use Steady\Engine\Components\AdvancedModel;
use Steady\Engine\Modules\Field\Models\FieldModel;
use Steady\Engine\SW;
use yii\helpers\Html;

class HtmlHelper
{
    /**
     * @name AdvancedModel $model
     * @return string
     * @throws \Exception
     */
    public static function generateFieldForm(AdvancedModel $model)
    {
        $result = '';
        $fields = $model->getFieldsForOwner();
        $data = $model->fields ?? null;
        if ($fields) {
            foreach ($fields as $field) {
                $field = new FieldModel($field['fieldRelation']);
                $value = !empty($data[$field->alias]) ? $data[$field->alias] : null;

                if ($field->type == FieldModel::TYPE_STRING) {
                    $result .= '<div class="form-group"><label>' . $field->title . '</label>'
                        . Html::input('text', "Fields[{$field->alias}]", $value, ['class' => 'form-control']) . '</div>';
                } else if ($field->type === 'text') {
                    $result .= '<div class="form-group"><label>' . $field->title . '</label>'
                        . Html::textarea("Fields[{$field->alias}]", $value, ['class' => 'form-control']) . '</div>';
                } else if ($field->type == FieldModel::TYPE_BOOLEAN) {
                    $result .= '<div class="checkbox"><label>'
                        . Html::checkbox("Fields[{$field->alias}]", $value, ['uncheck' => 0]) . ' ' . $field->title . '</label></div>';
                } else if ($field->type === 'select') {
                    $options = ['' => SW::t('admin', 'SelectWidget')];
                    foreach ($field->options as $option) {
                        $options[$option] = $option;
                    }
                    $result .= '<div class="form-group"><label>' . $field->title . '</label><select name="Fields[' . $field->alias . ']" class="form-control">'
                        . Html::renderSelectOptions($value, $options) . '</select></div>';
                } else if ($field->type === 'checkbox') {
                    $options = '';
                    foreach ($field->options as $option) {
                        $checked = $value && in_array($option, $value);
                        $options .= '<br><label>' . Html::checkbox("Fields[{$field->alias}][]", $checked, ['value' => $option])
                            . ' ' . $option . '</label>';
                    }
                    $result .= '<div class="checkbox well well-sm"><b>' . $field->title . '</b>' . $options . '</div>';
                } else if ($field->type == FieldModel::TYPE_TAGS) {
                    $result .= '<div class="form-group"><label>' . $field->title . '</label>'
                        . TagsInput::widget([
                            'name' => "Fields[{$field->alias}]",
                            'value' => $value,
                            'loadUrl' => $field->content,
                        ])
                        . '</div>';
                }
            }
        }
        return $result;
    }
}