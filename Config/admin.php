<?php

return [
    'class' => 'Steady\Admin\AdminModule',
    'aliases' => [
        '@admin-frontend' => STD_PATH_ADMIN . '/Resources/frontend',
    ],
    'modules' => [
        'parser' => [
            'class' => \Steady\Modules\Parser\ParserModule::class,
        ],
    ],
];